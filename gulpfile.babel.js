import gulp from 'gulp';
import autoprefixer from 'gulp-autoprefixer';
import babel from 'gulp-babel';
import browserSync from 'browser-sync';
import clean from 'gulp-clean';
import concat from 'gulp-concat';
import gulpif from 'gulp-if';
import neat from 'node-neat';
import minifyCss from 'gulp-minify-css';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';
import useref from 'gulp-useref';
import {stream as wiredep} from 'wiredep';

const reload = browserSync.reload;

// Inject bower components
gulp.task('bower', () => {
  gulp.src('source/styles/*.scss')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('source/styles'));

  gulp.src('source/*.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('source'));
});

// HTML
gulp.task('html', () => {
  var assets = useref.assets({ searchPath: ['.', 'source/assets'] });
  return gulp.src('source/*.html')
    .pipe(assets)
    .pipe(gulpif('*.js', uglify()))
    .pipe(gulpif('*.css', minifyCss()))
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulp.dest('build'));
});

// Styles
gulp.task('styles', () => {
  return gulp.src('source/assets/styles/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass.sync({
      includePaths: neat.includePaths
    }).on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['> 1%', 'last 2 versions', 'IE >= 9'] }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/assets/styles'))
    .pipe(reload({ stream: true }));
});

// Scripts
gulp.task('scripts', () => {
  return gulp.src('source/assets/scripts/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest('build/assets/scripts'));
});

// Images
gulp.task('images', () => {
  return gulp.src('source/assets/images/**/*')
    .pipe(gulp.dest('build/assets/images'));
});

// Fonts
gulp.task('fonts', () => {
  return gulp.src('source/assets/fonts/**/*')
    .pipe(gulp.dest('build/assets/fonts'));
});

// Extras
gulp.task('extras', () => {
  return gulp.src(['source/*.*', '!source/*.html'], {
    dot: true
  }).pipe(gulp.dest('build'));
});

// Serve
gulp.task('serve', ['build'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: 'build',
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch('source/*.html', ['html']).on('change', reload);
  gulp.watch('source/assets/styles/**/*.scss', ['styles']);
  gulp.watch('source/assets/scripts/**/*.js', ['scripts']).on('change', reload);
  gulp.watch('source/assets/images/**/*', ['images']).on('change', reload);
  gulp.watch('bower.json', ['bower']).on('change', reload);
});

// Cleanup
gulp.task('clean', () => {
  return gulp.src('build/*', { read: false })
    .pipe(clean());
});

// Build
gulp.task('build', ['html', 'extras', 'styles', 'scripts', 'images', 'fonts']);

// Default (serve)
gulp.task('default', ['clean'], () => {
  gulp.start('build');
});
