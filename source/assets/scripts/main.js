$(function ($) {
  'use strict';

  // Placeholder polyfill
  // https://github.com/mathiasbynens/jquery-placeholder
  $('input, textarea').placeholder();

  // Scroll to
  $('[data-scrollto]').on('click', function (event) {
    event.preventDefault();
    var target = $(this).data('scrollto');
    if (target || target === 0) {
      $(window).scrollTo(target, 800);
    }
  });

}(jQuery));
