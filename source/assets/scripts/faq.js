$(function ($) {
  'use strict';

  $('.faq').each(function () {
    var $faq = $(this);

    // Question toggle
    $faq.on('click', '.faq-question', function (event) {
      event.preventDefault();
      $(this).next('.faq-answer').slideToggle(200).end()
        .parent('li').toggleClass('collapsed');
    });
  });

}(jQuery));
